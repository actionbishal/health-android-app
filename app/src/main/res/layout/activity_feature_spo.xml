<!--
  ~ Copyright (c) 2015, Nordic Semiconductor
  ~ All rights reserved.
  ~
  ~ Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
  ~
  ~ 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  ~
  ~ 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
  ~ documentation and/or other materials provided with the distribution.
  ~
  ~ 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
  ~ software without specific prior written permission.
  ~
  ~ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  ~ LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  ~ HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  ~ LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ~ ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
  ~ USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  -->
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".spo.SPOActivity">

    <include
        android:id="@+id/toolbar_actionbar"
        layout="@layout/toolbar" />

    <com.android.mtelehealthuia.widget.ForegroundRelativeLayout
        style="@style/HeaderShadow"
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <!-- The size of text below must be fixed, therefore dp are used instead of sp -->

        <com.android.mtelehealthuia.widget.TrebuchetBoldTextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_centerVertical="true"
            android:layout_marginLeft="@dimen/spo2_feature_title_long_margin"
            android:rotation="270"
            android:text="@string/spo2_feature_title_long"
            android:textColor="@color/darkGray"
            android:textSize="32dp"
            android:textStyle="bold" />


        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:gravity="center_horizontal"
            android:orientation="vertical">

            <!-- Application section -->
            <LinearLayout
                android:id="@+id/indicator_pane"
                style="@style/Widget.List"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginLeft="@dimen/feature_horizontal_margin"
                android:layout_marginTop="15dp"
                android:layout_marginRight="@dimen/feature_horizontal_margin"
                android:orientation="vertical">

                <com.android.mtelehealthuia.widget.TrebuchetBoldTextView
                    style="@style/Widget.ListTitle"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:text="@string/spo_section_data_indicator_header" />
                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:orientation="horizontal"
                    android:padding="@dimen/feature_section_padding"
                    tools:ignore="UselessParent">

                    <com.android.mtelehealthuia.widget.TrebuchetTextView
                        android:id="@+id/data_indicator"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginBottom="42dp"
                        android:text="@string/oxymeter"
                        android:textSize="20sp"
                        android:textStyle="bold" />

                    <ImageView
                        android:id="@+id/oxymeter_indicator"
                        android:layout_width="25dp"
                        android:layout_height="25dp"
                        android:layout_marginLeft="30dp"
                        android:gravity="right"
                        android:src="@drawable/led_indicator">
                    </ImageView>
                </LinearLayout>
            </LinearLayout>

            <LinearLayout
                android:id="@+id/spo_pane"
                style="@style/Widget.List"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginLeft="@dimen/feature_horizontal_margin"
                android:layout_marginTop="15dp"
                android:layout_marginRight="@dimen/feature_horizontal_margin"
                android:orientation="vertical">

                <com.android.mtelehealthuia.widget.TrebuchetBoldTextView
                    style="@style/Widget.ListTitle"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:text="@string/spo_section_blood_oxygen_header" />

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal"
                    android:padding="@dimen/feature_section_padding">

                    <com.android.mtelehealthuia.widget.TrebuchetTextView
                        android:layout_width="0dp"
                        android:layout_height="match_parent"
                        android:layout_weight="1"
                        android:gravity="center_vertical"
                        android:text="@string/spo2" />

                    <com.android.mtelehealthuia.widget.TrebuchetBoldTextView
                        android:id="@+id/SpO2"
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:freezesText="true"
                        android:gravity="right"
                        android:text="@string/not_available_value"
                        android:textSize="36sp" />
                </LinearLayout>
            </LinearLayout>

            <LinearLayout
                android:id="@+id/spo_pulse_pane"
                style="@style/Widget.List"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginLeft="@dimen/feature_horizontal_margin"
                android:layout_marginTop="15dp"
                android:layout_marginRight="@dimen/feature_horizontal_margin"
                android:orientation="vertical">

                <com.android.mtelehealthuia.widget.TrebuchetBoldTextView
                    style="@style/Widget.ListTitle"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:text="@string/spo_section_pulse_header" />

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal"
                    android:padding="@dimen/feature_section_padding">

                    <com.android.mtelehealthuia.widget.TrebuchetTextView
                        android:layout_width="0dp"
                        android:layout_height="match_parent"
                        android:layout_weight="1"
                        android:gravity="center_vertical"
                        android:text="@string/spo_pulse" />

                    <com.android.mtelehealthuia.widget.TrebuchetBoldTextView
                        android:id="@+id/HeartRate"
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:freezesText="true"
                        android:gravity="right"
                        android:text="@string/not_available_value"
                        android:textSize="36sp" />
                </LinearLayout>
            </LinearLayout>
            <RelativeLayout
                android:id="@+id/done_layout"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_margin="10dp"
                android:visibility="invisible"
                android:layout_gravity="center">

                <android.support.design.widget.FloatingActionButton
                    android:id="@+id/img_done"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:src="@drawable/ic_done_24dp" />

                <com.android.mtelehealthuia.widget.TrebuchetTextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_below="@+id/img_done"
                    android:layout_centerHorizontal="true"
                    android:layout_margin="10dp"
                    android:text="Done" />
            </RelativeLayout>
        </LinearLayout>

        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_alignParentBottom="true"
            android:layout_centerHorizontal="true"
            android:layout_marginBottom="6dp"
            android:text="@string/mtelehealth_uia"/>

    </com.android.mtelehealthuia.widget.ForegroundRelativeLayout>
</LinearLayout>
