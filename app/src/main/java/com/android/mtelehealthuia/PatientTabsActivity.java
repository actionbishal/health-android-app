package com.android.mtelehealthuia;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import java.util.Objects;

import com.android.mtelehealthuia.database.LocalRecordsDatabaseHelper;
import com.android.mtelehealthuia.models.Patient;

public class PatientTabsActivity extends AppCompatActivity {
    private static final String TAG = PatientTabsActivity.class.getSimpleName();
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static int int_items = 2;
    Patient patient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_tabs);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Intent intent = this.getIntent();
        String patientID = String.valueOf(intent.getIntExtra("patientID", -1));
        Log.i(TAG, patientID);
        LocalRecordsDatabaseHelper mDBHelper = new LocalRecordsDatabaseHelper(this);
        patient = mDBHelper.getPatientByID(patientID);
        String fullName = patient.getFirstName() + " " + patient.getMiddleName() + " " + patient.getLastName();
        getSupportActionBar().setSubtitle(fullName);
        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tabs);
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        tabLayout.post(() -> tabLayout.setupWithViewPager(viewPager));
    }

    private class MyAdapter extends FragmentPagerAdapter {
        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FragmentPatientInfo(patient);

                case 1:
                    return new FragmentPatientVisits(patient);

            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        /**
         * This method returns the title of the tab according to the position.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Patient Info";
                case 1:
                    return "Visits";
            }
            return null;
        }
    }

//    public Patient getPatient(String patientID) {
//        Patient patient = new Patient();
//        LocalRecordsDatabaseHelper mDbHelper = new LocalRecordsDatabaseHelper(getApplicationContext());
//        SQLiteDatabase db = mDbHelper.getWritableDatabase();
//
//        String patientSelection = "_id = ?";
//        String[] patientArgs = {patientID};
//        String[] patientColumns = {"_id","openmrs_id", "first_name", "middle_name", "last_name",
//                "date_of_birth", "address1", "address2", "city_village", "state_province",
//                "postal_code", "country", "phone_number", "gender", "sdw", "occupation",
//                "patient_photo", "education_status"};
//        final Cursor idCursor = db.query("patient", patientColumns, patientSelection, patientArgs, null, null, null);
//
//        if (idCursor.moveToFirst()) {
//            do {
//                patient.setId(idCursor.getInt(idCursor.getColumnIndexOrThrow("_id")));
//                patient.setOpenmrs_patient_id(idCursor.getString(idCursor.getColumnIndexOrThrow("openmrs_id")));
//                patient.setFirstName(idCursor.getString(idCursor.getColumnIndexOrThrow("first_name")));
//                patient.setMiddleName(idCursor.getString(idCursor.getColumnIndexOrThrow("middle_name")));
//                patient.setLastName(idCursor.getString(idCursor.getColumnIndexOrThrow("last_name")));
//                patient.setDateOfBirth(idCursor.getString(idCursor.getColumnIndexOrThrow("date_of_birth")));
//                patient.setAddress1(idCursor.getString(idCursor.getColumnIndexOrThrow("address1")));
//                patient.setAddress2(idCursor.getString(idCursor.getColumnIndexOrThrow("address2")));
//                patient.setCityVillage(idCursor.getString(idCursor.getColumnIndexOrThrow("city_village")));
//                patient.setStateProvince(idCursor.getString(idCursor.getColumnIndexOrThrow("state_province")));
//                patient.setPostalCode(idCursor.getString(idCursor.getColumnIndexOrThrow("postal_code")));
//                patient.setCountry(idCursor.getString(idCursor.getColumnIndexOrThrow("country")));
//                patient.setPhoneNumber(idCursor.getString(idCursor.getColumnIndexOrThrow("phone_number")));
//                patient.setGender(idCursor.getString(idCursor.getColumnIndexOrThrow("gender")));
//                patient.setSdw(idCursor.getString(idCursor.getColumnIndexOrThrow("sdw")));
//                patient.setOccupation(idCursor.getString(idCursor.getColumnIndexOrThrow("occupation")));
//                patient.setPatientPhoto(idCursor.getString(idCursor.getColumnIndexOrThrow("patient_photo")));
//                patient.setEducation_level(idCursor.getString(idCursor.getColumnIndexOrThrow("education_status")));
//            } while (idCursor.moveToNext());
//        }
//        idCursor.close();
//        return patient;
//    }
@Override
public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
        onBackPressed();
        return true;
    }
    return super.onOptionsItemSelected(item);
}
}
