package com.android.mtelehealthuia.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.mtelehealthuia.models.Patient;
import com.android.mtelehealthuia.models.User;

import java.sql.Array;

/**
 * Class to manage input/output with the database.
 */

public class LocalRecordsDatabaseHelper extends SQLiteOpenHelper {
    SQLiteDatabase localdb;
    String FullName,User_name,password;
    int role;
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "localRecords.db";
    public static final String CREATE_PATIENT = "CREATE TABLE IF NOT EXISTS patient(" +
            "_id integer PRIMARY KEY," +
            "openmrs_uuid TEXT," +
            "openmrs_id TEXT," +
            "first_name TEXT," +
            "middle_name TEXT," +
            "last_name TEXT," +
            "date_of_birth TEXT," +
            "phone_number TEXT," +
            "address1 TEXT," +
            "address2 TEXT," +
            "city_village TEXT," +
            "state_province TEXT," +
            "postal_code TEXT," +
            "country TEXT," +
            "gender TEXT," +
            "sdw TEXT," + //Temporary
            "occupation TEXT," + //Temporary
            "patient_photo TEXT," +
            "education_status TEXT" +
            ")";
    public static final String CREATE_USER = "CREATE TABLE IF NOT EXISTS users(" +
            "_id integer PRIMARY KEY," +
            "full_name TEXT," +
            "username TEXT," +
            "password TEXT," +
            "role integer" +
            ")";
    public static final String CREATE_ATTRIB = "CREATE TABLE IF NOT EXISTS patient_attribute (" +
            "_id integer PRIMARY KEY," +
            "attribute_type_id integer(10) NOT NULL," +
            "patient_id integer  NOT NULL," +
            "value varchar(255)," +
            "FOREIGN KEY (patient_id) REFERENCES patient(patient_id)" +
            ")";
    public static final String CREATE_VISIT = "CREATE TABLE IF NOT EXISTS visit (" +
            "_id integer PRIMARY KEY," +
            "patient_id integer," +
            "start_datetime TEXT NOT NULL," +
            "end_datetime TEXT" +
            ")";
    public static final String CREATE_OBS = "CREATE TABLE IF NOT EXISTS obs (" +
            "_id integer PRIMARY KEY," +
            "patient_id integer," +
            "visit_id integer(10) NOT NULL," +
            "value text," +
            "concept_id integer(10) NOT NULL" +
            ")";
    //    public static final String CREATE_VISIT = "CREATE TABLE IF NOT EXISTS visit (" +
//            "_id integer PRIMARY KEY," +
//            "patient_id integer," +
//            "start_datetime TEXT NOT NULL," +
//            "end_datetime TEXT," +
//            "visit_type_id integer(10)," +
//            "visit_location_id integer(10) NOT NULL," +
//            "visit_creator TEXT NOT NULL," +
//            "openmrs_visit_uuid TEXT" +
//            ")";
//    public static final String CREATE_OBS = "CREATE TABLE IF NOT EXISTS obs (" +
//            "_id integer PRIMARY KEY," +
//            "patient_id integer," +
//            "visit_id integer(10) NOT NULL," +
//            "value text," +
//            "concept_id integer(10) NOT NULL," +
//            "creator TEXT," +
//            "openmrs_encounter_id integer(10)," +
//            "openmrs_obs_id integer(10)" +
//            ")";
//
//    public static final String CREATE_ENCOUNTER = "CREATE TABLE IF NOT EXISTS encounter ("+
//            "_id integer PRIMARY KEY," +
//            "openmrs_encounter_id integer(10) NOT NULL," +
//            "patient_id integer," +
//            "visit_id integer(10) NOT NULL," +
//            "openmrs_visit_uuid TEXT," +
//            "encounter_type TEXT," +
//            "encounter_provider TEXT" +
//            ")";
//
//    public static final String CREATE_USER = "CREATE TABLE IF NOT EXISTS user_provider (" +
//            "_id integer PRIMARY KEY," +
//            "openmrs_provider_id integer(10)," +
//            "openmrs_user_id integer(10)," +
//            "openmrs_role varchar(50)," +
//            "first_name varchar(50) NOT NULL," +
//            "middle_name varchar(50)," +
//            "last_name varchar(50)," +
//            "username varchar(50) NOT NULL," +
//            "password varchar(128) NOT NULL," +
//            "secret_question varchar(255) NOT NULL," +
//            "secret_answer varchar(255) NOT NULL," +
//            "date_created TEXT NOT NULL," +
//            "creator TEXT NOT NULL," +
//            "date_changed TEXT NOT NULL," +
//            "changed_by integer(10) NOT NULL" +
//            ")";
//
//    public static final String CREATE_PROVIDER = "CREATE TABLE IF NOT EXISTS providers (" +
//            "_id integer PRIMARY KEY," +
//            "openmrs_user_uuid integer(10)," +
//            "name varchar(50)" +
//            ")";
//
//    public static final String CREATE_LOCATION = "CREATE TABLE IF NOT EXISTS location_details (" +
//            "_id integer PRIMARY KEY," +
//            "tablet_id integer(10) NOT NULL," +
//            "location_name varchar(255) NOT NULL," +
//            "openmrs_location_id integer(255) NOT NULL" +
//            ")";
//
//    public static final String CREATE_DELAYED_JOBS = "CREATE TABLE IF NOT EXISTS " + DelayedJobQueueProvider.DELAYED_JOBS_TABLE_NAME + " (" +
//            DelayedJobQueueProvider._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
//            DelayedJobQueueProvider.JOB_TYPE + " TEXT NOT NULL," +
//            DelayedJobQueueProvider.JOB_PRIORITY + " INTEGER NOT NULL," +
//            DelayedJobQueueProvider.JOB_REQUEST_CODE + " INTEGER NOT NULL," +
//            DelayedJobQueueProvider.PATIENT_NAME + " TEXT NOT NULL," +
//            DelayedJobQueueProvider.PATIENT_ID + " INTEGER NOT NULL," +
//            DelayedJobQueueProvider.VISIT_ID + " TEXT," +
//            DelayedJobQueueProvider.VISIT_UUID + " TEXT," +
//            DelayedJobQueueProvider.STATUS + " INTEGER," +
//            DelayedJobQueueProvider.DATA_RESPONSE + " TEXT," +
//            DelayedJobQueueProvider.SYNC_STATUS + " INTEGER" +
//            ")";
    public static final String DROP = "DROP TABLE IF EXISTS";

    public static final String CREATE_IMAGE_RECORDS = "CREATE TABLE IF NOT EXISTS image_records(" +
            "_id integer PRIMARY KEY AUTOINCREMENT," +
            "patient_id integer NOT NULL," +
            "visit_id integer(10)," +
            "image_path TEXT NOT NULL," +
            "image_type TEXT NOT NULL," +
            "parse_id TEXT," +
            "delete_status integer NOT NULL"
            + ")";


    public LocalRecordsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON");
        db.execSQL(CREATE_PATIENT);
        db.execSQL(CREATE_ATTRIB);
        db.execSQL(CREATE_VISIT);
        db.execSQL(CREATE_OBS);
        db.execSQL(CREATE_USER);
//        db.execSQL(CREATE_ENCOUNTER);
//        db.execSQL(CREATE_USER);
//        db.execSQL(CREATE_PROVIDER);
//        db.execSQL(CREATE_LOCATION);
//        db.execSQL(CREATE_DELAYED_JOBS);
        db.execSQL(CREATE_IMAGE_RECORDS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: discuss upgrade policy
    }

    public Patient getPatientByID(String patientID) {
        Patient patient = new Patient();
        SQLiteDatabase db = getWritableDatabase();

        String patientSelection = "_id = ?";
        String[] patientArgs = {patientID};
        String[] patientColumns = {"_id", "openmrs_id", "first_name", "middle_name", "last_name",
                "date_of_birth", "address1", "address2", "city_village", "state_province",
                "postal_code", "country", "phone_number", "gender", "sdw", "occupation",
                "patient_photo", "education_status"};
        final Cursor idCursor = db.query("patient", patientColumns, patientSelection, patientArgs, null, null, null);

        if (idCursor.moveToFirst()) {
            do {
                patient.setId(idCursor.getInt(idCursor.getColumnIndexOrThrow("_id")));
                patient.setOpenmrs_patient_id(idCursor.getString(idCursor.getColumnIndexOrThrow("openmrs_id")));
                patient.setFirstName(idCursor.getString(idCursor.getColumnIndexOrThrow("first_name")));
                patient.setMiddleName(idCursor.getString(idCursor.getColumnIndexOrThrow("middle_name")));
                patient.setLastName(idCursor.getString(idCursor.getColumnIndexOrThrow("last_name")));
                patient.setDateOfBirth(idCursor.getString(idCursor.getColumnIndexOrThrow("date_of_birth")));
                patient.setAddress1(idCursor.getString(idCursor.getColumnIndexOrThrow("address1")));
                patient.setAddress2(idCursor.getString(idCursor.getColumnIndexOrThrow("address2")));
                patient.setCityVillage(idCursor.getString(idCursor.getColumnIndexOrThrow("city_village")));
                patient.setStateProvince(idCursor.getString(idCursor.getColumnIndexOrThrow("state_province")));
                patient.setPostalCode(idCursor.getString(idCursor.getColumnIndexOrThrow("postal_code")));
                patient.setCountry(idCursor.getString(idCursor.getColumnIndexOrThrow("country")));
                patient.setPhoneNumber(idCursor.getString(idCursor.getColumnIndexOrThrow("phone_number")));
                patient.setGender(idCursor.getString(idCursor.getColumnIndexOrThrow("gender")));
                patient.setSdw(idCursor.getString(idCursor.getColumnIndexOrThrow("sdw")));
                patient.setOccupation(idCursor.getString(idCursor.getColumnIndexOrThrow("occupation")));
                patient.setPatientPhoto(idCursor.getString(idCursor.getColumnIndexOrThrow("patient_photo")));
                patient.setEducation_level(idCursor.getString(idCursor.getColumnIndexOrThrow("education_status")));
            } while (idCursor.moveToNext());
        }
        idCursor.close();
        return patient;
    }

    public void createUser() {
        Log.i("log","called");
        localdb = getWritableDatabase();

        ContentValues UserEntries = new ContentValues();

        UserEntries.put("full_name", "Health worker");
        UserEntries.put("username", "healthworker");
        UserEntries.put("password", "123456");
        UserEntries.put("role", 1);
//        complaintEntries.put("creator", CREATOR_ID);


        localdb.insert("users", null, UserEntries);

        ContentValues UsersEntries = new ContentValues();

        UsersEntries.put("full_name", "Doctor worker");
        UsersEntries.put("username", "doctor");
        UsersEntries.put("password", "123456");
        UsersEntries.put("role", 2);
//        complaintEntries.put("creator", CREATOR_ID);
        localdb.insert("users", null, UsersEntries);
        Log.i("log","success");

    }


    public User fetchUser(String username,String password) {
        User user = new User();
        SQLiteDatabase db = getWritableDatabase();

        String selection = "username = ? AND password=?";
        String[] Args = {username,password};
        String[] columns = {"_id","username","role","full_name"};
        final Cursor idCursor = db.query("users", columns, selection, Args, null, null, null);

        if (idCursor.moveToFirst()) {
            do {
                user.set_id(idCursor.getInt(idCursor.getColumnIndexOrThrow("_id")));
                user.setUsername(idCursor.getString(idCursor.getColumnIndexOrThrow("username")));
                user.setRole(idCursor.getInt(idCursor.getColumnIndexOrThrow("role")));

            } while (idCursor.moveToNext());
        }
        idCursor.close();
        return user;
    }

}
