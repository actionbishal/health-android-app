package com.android.mtelehealthuia;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.android.mtelehealthuia.database.LocalRecordsDatabaseHelper;
import com.android.mtelehealthuia.models.Patient;
import com.android.mtelehealthuia.models.Visit;

public class FragmentPatientVisits extends Fragment {
    private static final String TAG = "FragmentPatientVisits";
    RecyclerView recyclerView;
    TextView emptyView;
    TextView new_visit;
    Integer patientID;
    String patientName;
    String visitID;
    String intentTag = "";
    Patient patient;
    public static String previsitValue;
    Button editbtn;
    Button newVisit;
    LinearLayout previousVisitsList;
    SharedPreferences.Editor e;
    SharedPreferences sharedPreferences;
    LocalRecordsDatabaseHelper mDBHelper;

    public FragmentPatientVisits() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentPatientVisits(Patient patient) {
        this.patient = patient;
        this.patientName = patient.getFirstName()+" "+patient.getLastName();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_visits, container, false);

        mDBHelper = new LocalRecordsDatabaseHelper(getContext());
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new VisitAdapter(getContext(), getAllVisits(), patient.getId(),patientName));
        emptyView = view.findViewById(R.id.text_empty_message);
        new_visit = view.findViewById(R.id.new_visit);
        new_visit.setOnClickListener(v -> {
            Intent intent2 = new Intent(getContext(), VitalsActivity.class);
//            String fullName = patient.getFirstName() + " " + patient.getLastName();
            intent2.putExtra("patientID", "" + patient.getId());

            SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
            Date todayDate = new Date();
            String thisDate = currentDate.format(todayDate);

            ContentValues visitData = new ContentValues();
            visitData.put("patient_id", patient.getId());
            Log.i(TAG, "onClick: " + thisDate);
            visitData.put("start_datetime", thisDate);

            LocalRecordsDatabaseHelper mDbHelper = new LocalRecordsDatabaseHelper(getContext());
            SQLiteDatabase localdb = mDbHelper.getWritableDatabase();
            Long visitLong = localdb.insert(
                    "visit",
                    null,
                    visitData
            );
            Log.i(TAG, String.valueOf(visitLong));

            visitID = String.valueOf(visitLong);
            localdb.close();
            intent2.putExtra("visitID", visitID);
            intent2.putExtra("name", patientName);
//            intent2.putExtra("name", fullName);
            intent2.putExtra("tag", "new");
            startActivity(intent2);
        });
        return view;
    }

    public ArrayList<Visit> getAllVisits() {
        String patientID = String.valueOf(patient.getId());
        ArrayList<Visit> allVisits = new ArrayList<>();
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        String visitSelection = "patient_id = ?";
        String[] visitArgs = {patientID};
        String[] visitColumns = {"_id", "start_datetime", "end_datetime"};
        String visitOrderBy = "_id";
        Log.i(TAG, "cursor" + patientID);

        Cursor cursor = db.query("visit", visitColumns, visitSelection, visitArgs, null, null, visitOrderBy);
        if (cursor != null && cursor.moveToFirst()) {
            Log.i(TAG, "cursor");

            do {
                Visit visit = new Visit();

                String date = cursor.getString(cursor.getColumnIndexOrThrow("start_datetime"));
                String end_date = cursor.getString(cursor.getColumnIndexOrThrow("end_datetime"));
                Integer visit_id = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));

                visit.setId(visit_id);
                visit.setStartDatetime(date);
                visit.setEndDatetime(end_date);
                allVisits.add(visit);
                Log.i(TAG, visit.getStartDatetime());
            } while (cursor.moveToNext());
        }
        cursor.close();
        return allVisits;
    }
}
