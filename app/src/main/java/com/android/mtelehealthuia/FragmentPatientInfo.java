package com.android.mtelehealthuia;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.mtelehealthuia.models.Patient;
import com.android.mtelehealthuia.utilities.HelperMethods;

public class FragmentPatientInfo extends Fragment {
    Patient patient;

    public FragmentPatientInfo() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentPatientInfo(Patient patient) {
        this.patient = patient;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_info, container, false);
        TextView patient_name = view.findViewById(R.id.patient_name);
        String fullName = patient.getFirstName() + " " + patient.getMiddleName() + " " + patient.getLastName();
        patient_name.setText(fullName);
        displayUI(view);
        return view;
    }

    public void displayUI(View view) {
        TextView idView = view.findViewById(R.id.textView_ID);
        TextView dobView = view.findViewById(R.id.textView_DOB);
        TextView ageView = view.findViewById(R.id.textView_age);
        TextView addr1View = view.findViewById(R.id.textView_address_1);
        TextView addr2View = view.findViewById(R.id.textView_address2);
        TextView addrFinalView = view.findViewById(R.id.textView_address_final);
        TextView education_statusView = view.findViewById(R.id.textView_education_status);
        TextView phoneView = view.findViewById(R.id.textView_phone);
        TextView sdwView = view.findViewById(R.id.textView_SDW);
        TextView occuView = view.findViewById(R.id.textView_occupation);
        TextView editPatient = view.findViewById(R.id.edit_patient);
        editPatient.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), PatientIdentificationActivity.class);
            intent.putExtra("tag", "edit");
            intent.putExtra("pid", patient.getId());
            startActivity(intent);
        });

        idView.setText("" + patient.getId());
        dobView.setText(patient.getDateOfBirth());
        int age = HelperMethods.getAge(patient.getDateOfBirth());
        ageView.setText(String.valueOf(age));
        addr1View.setText(patient.getAddress1());
        addr2View.setText(patient.getAddress2());
        String city_village;
        if (patient.getCityVillage() != null) {
            city_village = patient.getCityVillage().trim();
        } else {
            city_village = "";
        }

        String postal_code;
        if (patient.getPostalCode() != null) {
            postal_code = patient.getPostalCode().trim() + ",";
        } else {
            postal_code = "";
        }

        String addrFinalLine =
                String.format("%s, %s, %s %s",
                        city_village, patient.getStateProvince(),
                        postal_code, patient.getCountry());
        addrFinalView.setText(addrFinalLine);
        phoneView.setText(patient.getPhoneNumber());
        education_statusView.setText(patient.getEducation_level());
        sdwView.setText(patient.getSdw());
        occuView.setText(patient.getOccupation());
    }
}