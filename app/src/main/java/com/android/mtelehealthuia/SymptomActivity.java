package com.android.mtelehealthuia;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.Spinner;
import android.widget.Toast;

import static java.security.AccessController.getContext;

public class SymptomActivity extends AppCompatActivity {

    private String selectedSex = null;
    private Spinner selectBodyArea;
    private Spinner selectBodyPart;
    private Context context;
    private CheckedTextView[] allCheckedTextViews;
    private CheckedTextView male,female;

//    private OnSymptomActivityInteractionListener mListener;

    public SymptomActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflate the layout for this fragment
        setContentView(R.layout.activity_symptom);

        context = getApplicationContext();

//        SET ACTION BAR
        final Toolbar toolbar = findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (actionBar != null) {
            actionBar.setTitle("Symptom Analysis");
        }

        selectBodyArea = (Spinner) findViewById(R.id.selected_body_area);
        selectBodyPart = (Spinner) findViewById(R.id.selected_body_part);
        CheckedTextView male = (CheckedTextView) findViewById(R.id.radio_male);
        CheckedTextView female = (CheckedTextView) findViewById(R.id.radio_female);
        FloatingActionButton call108 = (FloatingActionButton) findViewById(R.id.call_108);
        allCheckedTextViews = new CheckedTextView[]{male, female};
//        submitButton.setSupportBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#E64A19")));

        selectBodyArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

//                selectedBodyArea = selectBodyArea.getSelectedItem().toString();
                String[] bodyPartList;
                ArrayAdapter<String> bodyPartListAdapter;
                switch (position) {
                    case 0:
                        if (selectBodyPart.getVisibility() == View.VISIBLE) {
                            selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, R.anim.sink_up));
                            selectBodyPart.setVisibility(View.GONE);
                        }
                        break;
                    case 1:
                        bodyPartList = getResources().getStringArray(R.array.head_area);
                        bodyPartListAdapter = new ArrayAdapter<>(
                                context, R.layout.spinner_item, bodyPartList
                        );
                        bodyPartListAdapter.setDropDownViewResource(R.layout.spinner_item);
                        bodyPartListAdapter.notifyDataSetChanged();
                        selectBodyPart.setAdapter(bodyPartListAdapter);
                        selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, R.anim.float_down));
                        selectBodyPart.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        bodyPartList = getResources().getStringArray(R.array.chest_area);
                        bodyPartListAdapter = new ArrayAdapter<>(
                                context, R.layout.spinner_item, bodyPartList
                        );
                        bodyPartListAdapter.setDropDownViewResource(R.layout.spinner_item);
                        bodyPartListAdapter.notifyDataSetChanged();
                        selectBodyPart.setAdapter(bodyPartListAdapter);
                        selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, R.anim.float_down));
                        selectBodyPart.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        bodyPartList = getResources().getStringArray(R.array.abdomen);
                        bodyPartListAdapter = new ArrayAdapter<>(
                                context, R.layout.spinner_item, bodyPartList
                        );
                        bodyPartListAdapter.setDropDownViewResource(R.layout.spinner_item);
                        bodyPartListAdapter.notifyDataSetChanged();
                        selectBodyPart.setAdapter(bodyPartListAdapter);
                        selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, R.anim.float_down));
                        selectBodyPart.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        bodyPartList = getResources().getStringArray(R.array.back_area);
                        bodyPartListAdapter = new ArrayAdapter<>(
                                context, R.layout.spinner_item, bodyPartList
                        );
                        bodyPartListAdapter.setDropDownViewResource(R.layout.spinner_item);
                        bodyPartListAdapter.notifyDataSetChanged();
                        selectBodyPart.setAdapter(bodyPartListAdapter);
                        selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, R.anim.float_down));
                        selectBodyPart.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        bodyPartList = getResources().getStringArray(R.array.pelvic);
                        bodyPartListAdapter = new ArrayAdapter<>(
                                context, R.layout.spinner_item, bodyPartList
                        );
                        bodyPartListAdapter.setDropDownViewResource(R.layout.spinner_item);
                        bodyPartListAdapter.notifyDataSetChanged();
                        selectBodyPart.setAdapter(bodyPartListAdapter);
                        selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, R.anim.float_down));
                        selectBodyPart.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        bodyPartList = getResources().getStringArray(R.array.arm);
                        bodyPartListAdapter = new ArrayAdapter<>(
                                context, R.layout.spinner_item, bodyPartList
                        );
                        bodyPartListAdapter.setDropDownViewResource(R.layout.spinner_item);
                        bodyPartListAdapter.notifyDataSetChanged();
                        selectBodyPart.setAdapter(bodyPartListAdapter);
                        selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, R.anim.float_down));
                        selectBodyPart.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        bodyPartList = getResources().getStringArray(R.array.legs);
                        bodyPartListAdapter = new ArrayAdapter<>(
                                context, R.layout.spinner_item, bodyPartList);
                        bodyPartListAdapter.setDropDownViewResource(R.layout.spinner_item);
                        bodyPartListAdapter.notifyDataSetChanged();
                        selectBodyPart.setAdapter(bodyPartListAdapter);
                        selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, R.anim.float_down));
                        selectBodyPart.setVisibility(View.VISIBLE);
                        break;
                    default:
                        selectBodyPart.setSelection(0);
                        selectBodyPart.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        selectBodyPart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    startActivityIfFormComplete();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickAction(view);
            }
        });
        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickAction(view);
            }
        });

        call108.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallEmergencyServices(getApplicationContext());
            }
        });

    }

    private void startActivityIfFormComplete() {
        if (selectedSex != null && selectBodyArea.getSelectedItemPosition() > 0 && selectBodyPart.getSelectedItemPosition() > 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onSymptomActivityInteraction(
                            selectedSex,
                            selectBodyArea.getSelectedItem().toString(),
                            selectBodyPart.getSelectedItem().toString());
                }
            }, 200);
        }
        if (selectedSex == null) {
            Toast.makeText(context, "Select gender to proceed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void clickAction(View v) {
        CheckedTextView temp = (CheckedTextView) v;
        if (temp != null) {
            if (!temp.isChecked()) {
                for (CheckedTextView item : allCheckedTextViews) {
                    item.setChecked(false);
                    item.setTextColor(Color.parseColor("#000000"));
                    item.setBackgroundColor(Color.parseColor("#FFFFFF"));

                }
                temp.setChecked(true);
                temp.setTextColor(Color.parseColor("#FFFFFF"));
                temp.setBackgroundColor(Color.parseColor("#AAAAAA"));
                selectedSex = temp.getText().toString();
                temp.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_selected));
                startActivityIfFormComplete();
            } else {
                temp.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_deselected));
            }
        }
    }
    private void resetViews(){
        selectBodyArea.setSelection(0);
//        selectBodyPart.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
//        for (CheckedTextView item : allCheckedTextViews) {
//            item.setChecked(false);
//            item.setTextColor(Color.parseColor("#000000"));
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        resetViews();
    }

//    interface OnSymptomActivityInteractionListener {
//        void onSymptomActivityInteraction(String selectedSex, String selectedBodyArea, String selectedBodyPart);
//    }
    public void onSymptomActivityInteraction(String selectedSex, String selectedBodyArea, String selectedBodyPart) {
        Intent intent = new Intent(this, SymptomCheck.class);
        intent.putExtra("selectedSex", selectedSex);
        intent.putExtra("selectedBodyArea", selectedBodyArea);
        intent.putExtra("selectedBodyPart", selectedBodyPart);
        startActivity(intent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);


    }

}
