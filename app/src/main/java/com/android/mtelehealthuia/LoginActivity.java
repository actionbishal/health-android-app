package com.android.mtelehealthuia;

import android.app.ProgressDialog;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.mtelehealthuia.database.LocalRecordsDatabaseHelper;
import com.android.mtelehealthuia.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {
    private EditText memail, mpassword;
    private ProgressDialog mprogressdialogue;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserRefrence;
    private FirebaseUser mCurrentUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        mprogressdialogue = new ProgressDialog(this);
        memail = findViewById(R.id.input_email);
        mpassword = findViewById(R.id.input_password);
        Button btn_login = findViewById(R.id.btn_login);
//

        TextView link_signup = findViewById(R.id.link_signup);
        link_signup.setOnClickListener(v -> {
            final Intent newIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(newIntent);
        });
        btn_login.setOnClickListener(v -> {
            String email = memail.getText().toString();
            String password = mpassword.getText().toString();
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (password.length() < 6) {
                Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {
                mprogressdialogue.setTitle("Logining In");
                mprogressdialogue.setMessage("Please Wait");
                mprogressdialogue.setCanceledOnTouchOutside(false);
                mprogressdialogue.show();
                loginUser(email, password);
            }

        });

    }

    private void loginUser(String email, String password) {

        LocalRecordsDatabaseHelper dbHelper = new LocalRecordsDatabaseHelper(this);
        User user = dbHelper.fetchUser(email, password);
        if (user.getRole() == 2) {
            Intent intent = new Intent(LoginActivity.this, DoctorActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else if (user.getRole() == 1){
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(LoginActivity.this,"Incorrect credentials.",Toast.LENGTH_LONG).show();
        }

        mprogressdialogue.dismiss();
//                            Toast.makeText(LoginActivity.this,dataSnapshot.toString(),Toast.LENGTH_LONG).show();

    }

    private void OldLoginUser(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
                    String current_uid = mCurrentUser.getUid();
                    mUserRefrence = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);
                    mUserRefrence.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String role = Objects.requireNonNull(dataSnapshot.child("Role").getValue()).toString();

                            if (role.equals("1")) {
                                Intent intent = new Intent(LoginActivity.this, DoctorActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }

                            mprogressdialogue.dismiss();
//                            Toast.makeText(LoginActivity.this,dataSnapshot.toString(),Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } else {
                    mprogressdialogue.dismiss();
                    Toast.makeText(LoginActivity.this, "Error occur", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
