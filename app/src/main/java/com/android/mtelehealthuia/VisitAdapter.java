package com.android.mtelehealthuia;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.android.mtelehealthuia.models.Visit;
import com.android.mtelehealthuia.utilities.HelperMethods;

public class VisitAdapter extends RecyclerView.Adapter<VisitAdapter.MyViewHolder> {

    private final Integer patientID;
    private LayoutInflater mInflater ;
    private ArrayList<Visit> visits;
    private int position;
    private static final String TAG = VisitAdapter.class.getSimpleName();
    private String patientName;

    VisitAdapter(Context context, ArrayList<Visit> visits,Integer patientID, String patientName){
        mInflater = LayoutInflater.from(context);
        this.visits = visits;
        this.patientID = patientID;
        this.patientName = patientName;
    }

    //called every time
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.visit_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Visit visit= visits.get(position);
        final String visitString = String.format("Seen on (%s)", HelperMethods.SimpleDatetoLongDate(visit.getStartDatetime()));
        holder.visit_date.setText(visitString);
        //to capture the position before the context menu is loaded:
        holder.itemView.setOnClickListener(v -> {
            setPosition(holder.getPosition());
            Intent intent = new Intent(holder.itemView.getContext(), SummaryActivity.class);
            intent.putExtra("visitID", ""+visit.getId());
            intent.putExtra("patientID", ""+patientID);
            intent.putExtra("name", patientName);
            Log.i(TAG, ""+visit.getId());
            Log.i(TAG, ""+patientID);
            Log.i(TAG, ""+patientName);
            holder.itemView.getContext().startActivity(intent);
        });
    }

    public int getItemPosition(){
        return position;
    }

    public void setPosition(int position){
        this.position = position;
    }


    @Override
    public int getItemCount() {
        return visits.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView visit_date;
        MyViewHolder(View itemView) {
            super(itemView);
            visit_date = itemView.findViewById(R.id.visit_date);
        }
    }
}