package com.android.mtelehealthuia;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.Sampler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity  {
    private ProgressDialog mprogress;
    private EditText mDisplayname;
    private EditText mEmail;
    private EditText mPassword;
    private Button mcreateBtn;
    private FirebaseAuth Auth;
    private DatabaseReference mdatabase;
    Spinner spinner;
    private CheckedTextView[] checkText_task;
    private CheckedTextView health_worker,doctor;
    private String selectedtask = null;
    private Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Button btn_login = findViewById(R.id.btn_reg);
        mDisplayname= findViewById(R.id.input_name);
        mEmail = findViewById(R.id.input_email);
         health_worker =  findViewById(R.id.radio_health_worker);
         doctor =  findViewById(R.id.radio_doctor);
        checkText_task = new CheckedTextView[]{health_worker, doctor};
        context = getApplicationContext();

        health_worker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAction(v);
            }
        });
        doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAction(v);
            }
        });
        mprogress = new ProgressDialog(this);
        Auth = FirebaseAuth.getInstance();
        mPassword= findViewById(R.id.input_password);
        btn_login.setOnClickListener(v -> {
            String name = mDisplayname.getText().toString();
            String email = mEmail.getText().toString();
            String password = mPassword.getText().toString();
//              String n1 = selectedtask.getBytes().toString();

            if (TextUtils.isEmpty(name)) {
                Toast.makeText(getApplicationContext(), "Enter your full name!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (password.length() < 6) {
                Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!TextUtils.isEmpty(name)|| !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {
                mprogress.setTitle("Regestering User");
                mprogress.setMessage("Please Wait");
                mprogress.setCanceledOnTouchOutside(true);
                mprogress.show();

//                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
//                    return;

                register_user(name, email, password);

        }
    });

}

    private  void register_user(final String name, final String email, final String password) {
        Auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        try {
                            //check if successful
                            if (task.isSuccessful()) {

                                FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                                String uid = current_user.getUid();
                                mdatabase= FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
                                HashMap<String,String> usermap = new HashMap<>();
                                usermap.put("name",name);
                                if (doctor.isChecked()){
                                    usermap.put("Role", "1" );
                                }else {
                                    usermap.put("Role","2");
                                }


                                mdatabase.setValue(usermap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            mprogress.dismiss();
                                            if (doctor.isChecked()){
                                                Intent intent = new Intent(RegisterActivity.this,DoctorActivity.class);
                                            startActivity(intent);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            }else {
                                                Intent intent = new Intent(RegisterActivity.this,HomeActivity.class);
                                                startActivity(intent);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                            }
//
                                        }
                                    }
                                });

                                //User is successfully registered and logged in
                                //start Profile Activity here
//                                Toast.makeText(RegisterActivity.this, "registration successful",
//                                        Toast.LENGTH_SHORT).show();
//                                finish();

                            }else{
                                mprogress.hide();
                                String s = "Sign up Failed" + task.getException();
                                Toast.makeText(RegisterActivity.this, s,
                                        Toast.LENGTH_SHORT).show();
//
                            }
                        }catch (Exception e){
                            e.printStackTrace();

                        }
                    }
                });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startActivityIfFormComplete() {

        if (selectedtask == null) {
            Toast.makeText(context, "Select gender to proceed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void clickAction(View v) {
        CheckedTextView temp = (CheckedTextView) v;
        if (temp != null) {
            if (!temp.isChecked()) {
                for (CheckedTextView item : checkText_task) {
                    item.setChecked(false);
                    item.setTextColor(Color.parseColor("#000000"));
                    item.setBackgroundColor(Color.parseColor("#FFFFFF"));

                }
                temp.setChecked(true);
                temp.setTextColor(Color.parseColor("#FFFFFF"));
                temp.setBackgroundColor(Color.parseColor("#AAAAAA"));
                selectedtask = temp.getText().toString();
                temp.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_selected));
                startActivityIfFormComplete();
            } else {
                temp.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_deselected));
            }
        }
    }

    }


