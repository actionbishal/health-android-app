package com.android.mtelehealthuia;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Objects;

import com.android.mtelehealthuia.database.LocalRecordsDatabaseHelper;
import com.android.mtelehealthuia.models.Obs;
import com.android.mtelehealthuia.models.Patient;
import com.android.mtelehealthuia.utilities.ConceptId;

import static com.android.mtelehealthuia.utilities.ConceptId.SPO2;

public class SummaryActivity extends AppCompatActivity {
    private static final String TAG = SummaryActivity.class.getSimpleName();
    Patient patient;
    String visitID, patientID, patientName;
    TextView heightView;
    TextView weightView;
    TextView pulseView;
    TextView bpView;
    TextView tempView;
    TextView spO2View;
    TextView bmiView;
    TextView mCnsview;
    TextView mRespview;
    TextView famHistView;
    TextView patHistView;
    TextView score_value;
    TextView dec;
    CardView score_card;
    TextView score_title;
    TextView score;
    String bp = "80";
    CardView sym_card;

    Obs famHistory = new Obs();
    Obs patHistory = new Obs();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        Toolbar toolbar = findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        setTitle("Summary");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Intent intent = this.getIntent();
        visitID = String.valueOf(intent.getStringExtra("visitID"));
        patientID = String.valueOf(intent.getStringExtra("patientID"));
        patientName = intent.getStringExtra("name");
        getSupportActionBar().setSubtitle(patientName);
        Log.i(TAG, "visitId:" + visitID);
        Log.i(TAG, "patientID:" + patientID);
        Log.i(TAG, "patientName:" + patientName);
        heightView = (TextView) findViewById(R.id.textView_height_value);
        weightView = (TextView) findViewById(R.id.textView_weight_value);
        pulseView = (TextView) findViewById(R.id.textView_pulse_value);
        bpView = (TextView) findViewById(R.id.textView_bp_value);
        tempView = (TextView) findViewById(R.id.textView_temp_value);
        spO2View = (TextView) findViewById(R.id.textView_pulseox_value);
        bmiView = (TextView) findViewById(R.id.textView_bmi_value);
        mRespview = (TextView) findViewById(R.id.textView_mResp_value);
        mCnsview = (TextView) findViewById(R.id.textView_mCns_value);
        famHistView = findViewById(R.id.textView_famhist_value);
        patHistView = findViewById(R.id.textView_medhist_value);
        score_card = findViewById(R.id.score_card);
        score_title = findViewById(R.id.score_title);
        score_value = findViewById(R.id.score_value);
        score = findViewById(R.id.score);
        dec = findViewById(R.id.dec);
        sym_card = findViewById(R.id.sym_card);
        sym_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SummaryActivity.this, SymptomActivity.class);
                startActivity(intent);
            }
        });

        TextView edit = findViewById(R.id.edit);
        edit.setOnClickListener(v -> {
            Intent intent1 = new Intent(SummaryActivity.this, VitalsActivity.class);
            intent1.putExtra("patientID", "" + patientID);
            intent1.putExtra("visitID", "" + visitID);
//                intent1.putExtra("name", patientName);
            //   intent.putStringArrayListExtra("exams", physicalExams);
            intent1.putExtra("tag", "edit");
            startActivity(intent1);
        });
        TextView editMedHist = findViewById(R.id.medical_history_edit);

        TextView editFamHist = findViewById(R.id.family_history_edit);

        editFamHist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder famHistDialog = new AlertDialog.Builder(SummaryActivity.this);
                famHistDialog.setTitle(getString(R.string.visit_summary_family_history));
                final LayoutInflater inflater = getLayoutInflater();
                View convertView = inflater.inflate(R.layout.dialog_edit_entry, null);
                famHistDialog.setView(convertView);

                final TextView famHistText = convertView.findViewById(R.id.textView_entry);
                if (famHistory.getValue() != null)
                    famHistText.setText(Html.fromHtml(famHistory.getValue()));
                famHistText.setEnabled(false);

                famHistDialog.setPositiveButton(getString(R.string.generic_manual_entry), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final AlertDialog.Builder textInput = new AlertDialog.Builder(SummaryActivity.this);
                        textInput.setTitle(R.string.question_text_input);
                        final EditText dialogEditText = new EditText(SummaryActivity.this);
                        if (famHistory.getValue() != null)
                            dialogEditText.setText(Html.fromHtml(famHistory.getValue()));
                        else
                            dialogEditText.setText("");
                        textInput.setView(dialogEditText);
                        textInput.setPositiveButton(R.string.ok, (dialog, which) -> {
                            famHistory.setValue(dialogEditText.getText().toString());
                            if (famHistory.getValue() != null) {
                                famHistText.setText(Html.fromHtml(famHistory.getValue()));
                                famHistView.setText(Html.fromHtml(famHistory.getValue()));
                            }

                            updateDatabase(famHistory.getValue(), ConceptId.RHK_FAMILY_HISTORY_BLURB);
                            dialog.dismiss();
                        });
                        textInput.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        textInput.show();
                        dialogInterface.dismiss();
                    }
                });

                famHistDialog.setNeutralButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                famHistDialog.setNegativeButton(R.string.generic_erase_redo, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent intent1 = new Intent(SummaryActivity.this, FamilyHistoryActivity.class);
                        intent1.putExtra("patientID", "" + patientID);
                        intent1.putExtra("visitID", "" + visitID);
                        intent1.putExtra("name", patientName);
                        intent1.putExtra("tag", "edit");
                        startActivity(intent1);
                        dialogInterface.dismiss();
                    }
                });

                famHistDialog.show();
            }
        });
        editMedHist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder historyDialog = new AlertDialog.Builder(SummaryActivity.this);
                historyDialog.setTitle(getString(R.string.visit_summary_medical_history));
                final LayoutInflater inflater = getLayoutInflater();
                View convertView = inflater.inflate(R.layout.dialog_edit_entry, null);
                historyDialog.setView(convertView);

                final TextView historyText = (TextView) convertView.findViewById(R.id.textView_entry);
                if (patHistory.getValue() != null)
                    historyText.setText(Html.fromHtml(patHistory.getValue()));
                historyText.setEnabled(false);

                historyDialog.setPositiveButton(getString(R.string.generic_manual_entry), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final AlertDialog.Builder textInput = new AlertDialog.Builder(SummaryActivity.this);
                        textInput.setTitle(R.string.question_text_input);
                        final EditText dialogEditText = new EditText(SummaryActivity.this);
                        if (patHistory.getValue() != null)
                            dialogEditText.setText(Html.fromHtml(patHistory.getValue()));
                        else
                            dialogEditText.setText("");
                        textInput.setView(dialogEditText);
                        textInput.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                patHistory.setValue(dialogEditText.getText().toString());
                                if (patHistory.getValue() != null) {
                                    historyText.setText(Html.fromHtml(patHistory.getValue()));
                                    patHistView.setText(Html.fromHtml(patHistory.getValue()));
                                }
                                updateDatabase(patHistory.getValue(), ConceptId.RHK_MEDICAL_HISTORY_BLURB);
                                dialog.dismiss();
                            }
                        });
                        textInput.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        textInput.show();
                        dialogInterface.dismiss();
                    }
                });

                historyDialog.setNegativeButton(getString(R.string.generic_erase_redo), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent1 = new Intent(SummaryActivity.this, PastMedicalHistoryActivity.class);
                        intent1.putExtra("patientID", "" + patientID);
                        intent1.putExtra("visitID", "" + visitID);
                        intent1.putExtra("name", patientName);
                        intent1.putExtra("tag", "edit");
                        startActivity(intent1);
                        dialogInterface.dismiss();
                    }
                });

                historyDialog.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                historyDialog.show();
            }
        });

    }

    private void queryObservations() {
        LocalRecordsDatabaseHelper mDBHelper = new LocalRecordsDatabaseHelper(getApplicationContext());
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        String[] columns = {"value", " concept_id"};
        String orderBy = "visit_id";
        String visitSelection = "patient_id = ? AND visit_id = ?";
        String[] visitArgs = {patientID, visitID};
        Cursor visitCursor = db.query("obs", columns, visitSelection, visitArgs, null, null, orderBy);
        if (visitCursor.moveToFirst()) {
            do {
                int dbConceptID = visitCursor.getInt(visitCursor.getColumnIndex("concept_id"));
                String dbValue = visitCursor.getString(visitCursor.getColumnIndex("value"));
                displayUI(dbConceptID, dbValue);
            } while (visitCursor.moveToNext());
        }
        visitCursor.close();
    }

    private void displayUI(int concept_id, String value) {
        switch (concept_id) {
            case ConceptId.HEIGHT: //Height
            {
                heightView.setText(value);
                break;
            }
            case ConceptId.WEIGHT: //Weight
            {
                weightView.setText(value);
                break;
            }
            case ConceptId.PULSE: //Pulse
            {
                pulseView.setText(value);
                break;
            }
            case ConceptId.SYSTOLIC_BP: //Systolic BP
            {
                bp = value;
                bpView.setText(value);
                break;
            }
            case ConceptId.DIASTOLIC_BP: //Diastolic BP
            {
                bpView.setText(bpView.getText() + "/" + value);
                break;
            }
            case ConceptId.TEMPERATURE: //Temperature
            {
                tempView.setText(value);
                break;
            }
            case SPO2: //SpO2
            {
                spO2View.setText(value);
                break;
            }
            case ConceptId.RESPIRATORY://respiratory
            {
                mRespview.setText(value);
                break;
            }
            case ConceptId.RHK_FAMILY_HISTORY_BLURB:
            {
                if (value != null)
                    famHistView.setText(Html.fromHtml(value));
                break;
            }

            case ConceptId.RHK_MEDICAL_HISTORY_BLURB:
            {
                if (value != null)
                    patHistView.setText(Html.fromHtml(value));
                break;
            }
            case ConceptId.CNS://resporatory
            {
                mCnsview.setText(value);
                break;
            }

            default:
                Log.i(TAG, "parseData: " + value);
                break;

        }
    }

    /**
     * This method updates patient details to database.
     *
     * @param string    variable of type String
     * @param conceptID variable of type int
     */

    private void updateDatabase(String string, int conceptID) {
        LocalRecordsDatabaseHelper mDbHelper = new LocalRecordsDatabaseHelper(this);
        SQLiteDatabase localdb = mDbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("value", string);

        String selection = "patient_id = ? AND visit_id = ? AND concept_id = ?";
        String[] args = {String.valueOf(patientID), visitID, String.valueOf(conceptID)};

        localdb.update(
                "obs",
                contentValues,
                selection,
                args
        );

    }

    private int calculateCnsScore(String cns) {

        double centralNervousSystem = Integer.parseInt(cns);

        int sum = 0;
        if (centralNervousSystem == 1) {
            sum = +3;
        } else if (centralNervousSystem == 0) {
            sum = +0;
        }

        return sum;

    }


    private int calculateResScore(String res) {

        double respirationRate = Integer.parseInt(res);

        int sum = 0;

        if (respirationRate <= 8) {
            sum += 3;
        } else if ((respirationRate >= 8.1) && (respirationRate <= 11)) {
            sum = +1;
        } else if ((respirationRate >= 11.1) && (respirationRate <= 20)) {
            sum = +0;
        } else if ((respirationRate >= 20.1) && (respirationRate <= 24)) {
            sum = +2;
        } else if ((respirationRate >= 24.1) && (respirationRate <= 40)) {
            sum = +3;
        }

        return sum;
    }


    private int calculatespScore(String spo2) {

        int sum = 0;

        double bloodOxygen = Integer.parseInt(spo2);
        if (bloodOxygen <= 91) {
            sum += 3;
        } else if ((bloodOxygen >= 91.1) && (bloodOxygen <= 93)) {
            sum = +2;
        } else if ((bloodOxygen >= 93.1) && (bloodOxygen <= 95)) {
            sum = +1;
        } else if ((bloodOxygen >= 95.1) && (bloodOxygen <= 100)) {
            sum = +0;
        }
        return sum;


    }

    private int calculateBpScore(String bp) {

        double bloodPressure = Integer.parseInt(bp);

        int sum = 0;

        if (bloodPressure <= 90) {
            sum += 3;
        } else if ((bloodPressure >= 90.1) && (bloodPressure <= 100)) {
            sum = +2;
        } else if ((bloodPressure >= 100.1) && (bloodPressure <= 110)) {
            sum = +1;
        } else if ((bloodPressure >= 110.1) && (bloodPressure <= 219)) {
            sum = +0;
        } else if ((bloodPressure >= 219.1) && (bloodPressure <= 400)) {
            sum += 3;
        }

        return sum;


    }

    private int calculateHeartScore(String hr) {

        double pulseRate = Integer.parseInt(hr);

        int sum = 0;

        if (pulseRate <= 40) {
            sum += 3;
        } else if ((pulseRate >= 40.1) && (pulseRate <= 50)) {
            sum = +1;
        } else if ((pulseRate >= 50.1) && (pulseRate <= 90)) {
            sum = +0;
        } else if ((pulseRate >= 90.1) && (pulseRate <= 100)) {
            sum = +1;
        } else if ((pulseRate >= 100.1) && (pulseRate <= 130)) {
            sum = +2;
        } else if ((pulseRate >= 130.1) && (pulseRate <= 200)) {
            sum += 3;
        }

        return sum;


    }

    private int calculateTempScore(String temp) {


        double bodyTemerature = Double.parseDouble(temp);


        int sum = 0;

        if (bodyTemerature <= 35) {
            sum += 3;
        } else if ((bodyTemerature >= 35.1) && (bodyTemerature <= 36)) {
            sum = +1;
        } else if ((bodyTemerature >= 36.1) && (bodyTemerature <= 38)) {
            sum = +0;

        } else if ((bodyTemerature >= 38.1) && (bodyTemerature <= 39)) {
            sum = +1;

        } else if ((bodyTemerature >= 39.1) && (bodyTemerature <= 40)) {
            sum = +2;
        }
        return sum;
    }

    private void setResult(int res, int decide) {
        score_title.setTextColor(Color.WHITE);
        score_value.setTextColor(Color.WHITE);
        dec.setTextColor(Color.WHITE);
        score.setTextColor(Color.WHITE);
        score.setText("" + res);

        if (res <= 4 && decide == 1) {
            score_value.setText("Low score: Assessment by a competent registered nurse who should decide if a change to frequency of clinical monitoring or an escalation of clinical care is required.");
            dec.setText("Assigned when an extreme variation in a single physiological parameter is present (i.e., a score of 3 in any one physiological parameter.");
            score_card.setBackgroundColor(Color.parseColor("#61bd4f"));

        } else if (res <= 4) {
            score_value.setText("Low score: Assessment by a competent registered nurse who should decide if a change to frequency of clinical monitoring or an escalation of clinical care is required.");

        } else if (res >= 4.1 && res <= 6 && decide == 1) {
            score_value.setText("Medium score: Urgent review by a clinician skilled with competencies in the assessment of acute illness.");
            dec.setText("Assigned when an extreme variation in a single physiological parameter is present (i.e., a score of 3 in any one physiological parameter.");
            score_card.setBackgroundColor(Color.parseColor("#fdb732"));

        } else if (res >= 4.1 && res <= 6) {

            score_value.setText("Medium score: Urgent review by a clinician skilled with competencies in the assessment of acute illness.");
            score_card.setBackgroundColor(Color.parseColor("#fdb732"));

        } else if (res >= 7 && res <= 30 && decide == 1) {
            score_value.setText("High score: Emergency assessment by a clinical/critical care outreach team with critical-care competencies and usually transfer of the patient to a higher dependency care area.");
            dec.setText("Assigned when an extreme variation in a single physiological parameter is present (i.e., a score of 3 in any one physiological parameter.");
            score_card.setBackgroundColor(Color.parseColor("#e6313b"));

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = this.getIntent();
        visitID = String.valueOf(intent.getStringExtra("visitID"));
        patientID = String.valueOf(intent.getStringExtra("patientID"));
        patientName = String.valueOf(intent.getStringExtra("name"));
        Log.i(TAG, visitID);
        Log.i(TAG, patientID);
        queryObservations();
        calculateBMI();

        if (!(tempView.getText().toString().isEmpty() || pulseView.getText().toString().isEmpty() || bp.isEmpty() || spO2View.getText().toString().isEmpty())) {

            int res = calculateTempScore(tempView.getText().toString()) + calculateHeartScore(pulseView.getText().toString()) + calculateBpScore(bp) + calculatespScore(spO2View.getText().toString()) + calculateResScore(mRespview.getText().toString()) + calculateCnsScore(mCnsview.getText().toString());
//        String res = Integer.toString(a);

            int decide;
            if (calculateTempScore(tempView.getText().toString()) == 3 || calculateHeartScore(pulseView.getText().toString()) == 3 || calculateBpScore(bp) == 3 || calculatespScore(spO2View.getText().toString()) == 3 || calculateResScore(mRespview.getText().toString()) == 3 || calculateCnsScore(mCnsview.getText().toString()) == 3) {
                decide = 1;
            } else {
                decide = 0;

            }

            setResult(res, decide);
        }

    }

    public void calculateBMI() {
        if (!(heightView.getText().toString().equals("") && weightView.getText().toString().equals(""))) {
            double numerator = Double.parseDouble(weightView.getText().toString()) * 10000;
            double denominator = (Double.parseDouble(heightView.getText().toString())) * (Double.parseDouble(heightView.getText().toString()));
            double bmi_value = numerator / denominator;
            DecimalFormat df = new DecimalFormat("0.00");
            bmiView.setText(df.format(bmi_value));
            //mBMI.setText(String.format(Locale.ENGLISH, "%.2f", bmi_value));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);


    }

}
